# * Author: torsw
# * Github: github.com/torswq
# * License: MIT LICENSE
#
# Begin license text
# * Copyright 2020 Lilypad Software
#
# * Permission is hereby granted, free of charge, to any person obtaining
# * a copy of this software and associated documentation files (the "Software"),
# * to deal in the Software without restriction, including without limitation
# * the rights to use, copy, modify, merge, publish, distribute, sublicense,
# * and/or sell copies of the Software, and to permit persons to whom the
# * Software is furnished to do so, subject to the following conditions:
#
# * The above copyright notice and this permission notice shall be included
# * in all copies or substantial portions of the Software.
#
# * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# * DEALINGS IN THE SOFTWARE.
# Ends license text
# * --------------------------------------------------------------
import random, os, re
from . import errlib, utilib

# This will be implemented on later versions...
#
#MAYUS_ALPHABET = {"A":1, "B":2, "C":3, "D":4, "E":5, "F":6,
#                  "G":7, "H":8, "I":9, "J":10,"K":11,"L":12,
#                  "M":13,"N":14,"Ñ":15,"O":16,"P":17,"Q":18,
#                  "R":19,"S":20,"T":21,"U":22,"V":23,"W":24,
#                  "X":25,"Y":26,"Z":27}
#
#MINUS_ALPHABET = {"a":1, "b":2, "c":3, "d":4, "e":5, "f":6,
#                  "g":7, "h":8, "i":9, "j":10,"k":11,"l":12,
#                  "m":13,"n":14,"ñ":15,"o":16,"p":17,"q":18,
#                  "r":19,"s":20,"t":21,"u":22,"v":23,"w":24,
#                  "x":25,"y":26,"z":27,}
def CSPRNgen():
    """Returns a system random Crypto Secure Pseudo Random Number
       Version = 0.1"""
    generator = random.SystemRandom()
    prn=int(repr(generator.random())[2:4])
    while prn > 127:
        prn = modulate(prn)
    return int(prn)

def modulate(number):
    """ "...Modulate you moron!"
    Function that ensures, your otp character does not exceed 255"""
    try:
        if type(number) == int or type(number) == float:
            if number+127 > 255:
                number=number%129
            else:
                return number
        else:
            raise errlib.InvalidDataType(number)
    except errlib.InvalidDataType as invalid_input:
        print("Invalid datatype: %s"%invalid_input.value)
    finally:
        return 0x0017

def OneTimePadGen(n=1):
    """One time pad generator
    n: length of the otp

    This function utilize OS dependant CSPRN Generator
    the number returned is not major to 27
    i.e Windows will use CryptGenRandom.
        Linux will use random/urandom."""
    OTP = []
    for i in range(n):
        OTP.append(CSPRNgen())
    return OTP

def cipher(string,otp):
    """In order to encrypt something you must give the string and the One Time Pad
    string: Text or string
    otp: One time pad list"""
    translated_str = []
    otpitems_used  = otp[0:len(string)]
    del otp[0:len(string)]
    for i in range(len(string)):
        translated_str.append(ord(string[i]))
    for i in range(len(translated_str)):
        translated_str[i] = translated_str[i]+otpitems_used[i]
    translated_str = utilib.toString(li=translated_str)
    encrypted_string = "-".join(translated_str)
    return (encrypted_string,otp)

def decipher(string,otp):
    string=string.split("-")
    string = utilib.toInteger(string)
    for i in range(len(string)):
        string[i]= chr(string[i]-otp[i])
    deciphered_string = "".join(string)
    string = utilib.toString(li=string)
    return (deciphered_string,otp)

if __name__ == "__main__":
    print("This is a library, if you want to try Camigner encryption system you must open test.exe")
    os.system("pause")
    exit()
