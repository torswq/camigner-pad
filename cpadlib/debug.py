# * Author: torsw
# * Github: github.com/torswq
# * License: MIT LICENSE
#
# Begin license text
# * Copyright 2020 Lilypad Software
#
# * Permission is hereby granted, free of charge, to any person obtaining
# * a copy of this software and associated documentation files (the "Software"),
# * to deal in the Software without restriction, including without limitation
# * the rights to use, copy, modify, merge, publish, distribute, sublicense,
# * and/or sell copies of the Software, and to permit persons to whom the
# * Software is furnished to do so, subject to the following conditions:
#
# * The above copyright notice and this permission notice shall be included
# * in all copies or substantial portions of the Software.
#
# * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# * DEALINGS IN THE SOFTWARE.
# Ends license text
# * ---------------------------------------
import math, cryptolib

def DEBUG_CSPRNgen(n,complexity=0,deepScan=False,output=False):
    """Debug CSPRN Generator.
This function will return how much duplicated numbers are in two list of n items."""
    list_a = []
    list_b = []
    dup_avg = []
    duplicated = 0
    #Obtain two list of n CSPRN
    for i in range(n):
        list_a.append(cryptolib.CSPRNgen())
    for i in range(n):
        list_b.append(cryptolib.CSPRNgen())
    for i in range(n):
        #Count how much duplicated there are
        if list_a[i] == list_b[i]:
            duplicated+=1
    return duplicated

def DEBUG_CSPRN_DUPAVG(n,complexity=1,deepScan=False,output=False):
    """Debug CSPRN Generator; returns the duplicated average number with x given complexity and passing n to DEBUG_CSPRNgen
    n: This argument is taken by DEBUG_CSPRNgen
    complexity: Level of complexity
        1: 1*256 scans
        2: 2*256 scans
        3: 3*256 scans
        4: 4*256 scans
    deepScan: Enable deep scan, scan the average number in the specified complexity level
        i.e 1: 32*256 (8192 scans)
            2: 64*256 (16384 scans)
            3: 128*512(65536 scans)
            4: 256*512(131072 scans)
    output: Enable an user friendly outpu"""
    if deepScan == False:
        tries_num = complexity*256
    elif deepScan == True and complexity == 1:
        tries_num = 32*256
    elif deepScan == True and complexity == 2:
        tries_num = 32*256
    elif deepScan == True and complexity == 3:
        tries_num = 128*512
    elif deepScan == True and complexity == 4:
        tries_num = 256*512
    dup_numbs = []
    final_num   = 0
    avg_number  = 0
    #Append the duplicated numbers returned by DEBUG_CSPRNgen to a list
    for i in range(tries_num):
        dup_numbs.append(DEBUG_CSPRNgen(n))
    #final_num is the sigma sum of all of the duplicated numbers returned by DEBUG_CSPRNgen
    final_num = math.fsum(dup_numbs)
    #...and avg_number is the final_num divided by the length of the duped numbers
    avg_number = final_num/n
    if output:
        print("LENGTH OF THE COMPARISON LISTS: %d\nNUMBER OF SCANS: %d\nAVERAGE DUPLICATED: %f"%(n,tries_num,avg_number))
    return (final_num,avg_number)
