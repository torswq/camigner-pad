# * Author: torsw
# * Github: github.com/torswq
# * License: MIT LICENSE
#
# Begin license text
# * Copyright 2020 Lilypad Software
#
# * Permission is hereby granted, free of charge, to any person obtaining
# * a copy of this software and associated documentation files (the "Software"),
# * to deal in the Software without restriction, including without limitation
# * the rights to use, copy, modify, merge, publish, distribute, sublicense,
# * and/or sell copies of the Software, and to permit persons to whom the
# * Software is furnished to do so, subject to the following conditions:
#
# * The above copyright notice and this permission notice shall be included
# * in all copies or substantial portions of the Software.
#
# * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# * DEALINGS IN THE SOFTWARE.
# Ends license text
# * ----------------------------------------------------------------------------
ERRCODES= ( 0x0001,0x0002,0x0003,0x0004,
            0x0005,0x0006,0x0007,0x0008,
            0x0009,0x0010,0x0012,0x0013,
            0xDEAD,0x007f,0x0016,0x0017)
CONTACT_1= "please send detailed information of this error to torsw@protonmail.com"
ERRMSG  = { 0x0001:"Unable to complete the operation, %s"%CONTACT_1,
            0x0002:"Unable to retrieve the mouse position",
            0x0003:"Unable to retrieve the mouse position (x coordinates)",
            0x0004:"Unable to retrieve the mouse position (y coordinates)",
            0x0005:"Unable to generate the MN number, (Read the documentation for further information)",
            0x0006:"Unable to compute the k number (Read the docs for further information)",
            0x0007:"Unable to find the urandom funcion, are you running a GNU/Linux environment?",
            0x0008:"Unable to find the random funcion, are you running a GNU/Linux environment?",
            0x0009:"Unable to find a CSPRG on the current environment",
            0x0010:"Unable to compute the FDK (Final Decimal K), are you using the right python version?",
            0x0011:"Unable to hash MN number, are you using the right python version?",
            0x0012:"Unable to hash FK, do you have installed SHA-512 libraries?",
            0x0013:"Error splitting the One Time Pad",
            0xDEAD:"Unhandled error exception, please report this error to github or contact the developer",
            0x007f:"Unhandled memory error, %s"%CONTACT_1,
            0x0016:"Invalid error code, %s"%CONTACT_1,
            0x0017:"Invalid datatype"
          }
class InvalidDataType(Exception):
    def __init__(self,value):
        self.value = value
    """Exception caused by invalid input data"""
    def __str__(self):
        return repr(self.value)

def getErrorByCode(code):
    try:
        return ERRMSG[code]
    except KeyError:
        return ERRMSG[0x0016]
