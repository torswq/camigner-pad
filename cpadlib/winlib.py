# * Author: torsw
# * Github: github.com/torswq
# * License: MIT LICENSE
#
# Begin license text
# * Copyright 2020 Lilypad Software
#
# * Permission is hereby granted, free of charge, to any person obtaining
# * a copy of this software and associated documentation files (the "Software"),
# * to deal in the Software without restriction, including without limitation
# * the rights to use, copy, modify, merge, publish, distribute, sublicense,
# * and/or sell copies of the Software, and to permit persons to whom the
# * Software is furnished to do so, subject to the following conditions:
#
# * The above copyright notice and this permission notice shall be included
# * in all copies or substantial portions of the Software.
#
# * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# * DEALINGS IN THE SOFTWARE.
# Ends license text
# * ----------------------------------------------------------------------------
import threading,random,os,time
import corelib, msvcrt
from ctypes.wintypes import LPPOINT, POINT

DELAY = 0.2
def getMousePos():
    t_getMousePos = threading.currentThread()
    times_captured= 0
    number= []
    print("Capturing your mouse position every %.2f seconds\nplease perform random movements with your cursor"%DELAY)
    while getattr(t_getMousePos,"do_run", True):
        time.sleep(DELAY)
        print("A")
        times_captured+=1
    print("Mouse position capture succed, with %d numbers stored"%times_captured)
    return 0
def captureMouse():
    print("You are about to generate the Mouse Number (MN), enter c or continue to continue with the process")
    inp = input(" ")
    if inp in ("c","continue"):
        pass
    else:
         return
    t_getMousePos = threading.Thread(target=getMousePos, name="t_getMousePos - del=%.2f"%DELAY)
    t_getMousePos.start()
    if not msvcrt.kbhit() == None:
        t_getMousePos.do_run = False

captureMouse()
