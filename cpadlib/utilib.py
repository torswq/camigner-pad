# * Author: torsw
# * Github: github.com/torswq
# * License: MIT LICENSE
#
# Begin license text
# * Copyright 2020 Lilypad Software
#
# * Permission is hereby granted, free of charge, to any person obtaining
# * a copy of this software and associated documentation files (the "Software"),
# * to deal in the Software without restriction, including without limitation
# * the rights to use, copy, modify, merge, publish, distribute, sublicense,
# * and/or sell copies of the Software, and to permit persons to whom the
# * Software is furnished to do so, subject to the following conditions:
#
# * The above copyright notice and this permission notice shall be included
# * in all copies or substantial portions of the Software.
#
# * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# * DEALINGS IN THE SOFTWARE.
# Ends license text
# * --------------------------------------------------------------

def toInteger(string,li=[]):
    """Convert string to integer.
    string: This value must be a string
    li: This value must contain a letter by letter string"""
    if not string == "":
        for i in range(len(string)):
            li.append(string[i])
    for i in range(len(li)):
        li[i]=int(li[i])
    return li

def toString(li=[]):
    """In order to convert numbers to integer you must pass a list of them"""
    for i in range(len(li)):
        li[i]=str(li[i])
    return li
