# Camigner pad
-----------------------------
## Description
Camigner pad is a pure python writenn library that let you generate One-Time pads (OTP) and encrypt content on it. The script uses random.SystemRandom for the CSPRN generation, this means that the entropy is OS-Dependant  
  
**License: MIT**  
**VERSION: 0.1**  

### Documentation

There is no documentation as of yet. But in the following versions i will  
upload a simple guide.

#### Installation
-----------------------------------
The installation is quite simple, just extract the cpadlib in your project/script folder.  
    Your script directory should look something like this  
    
![Directory example](/img/example.png)
  
After that you can use all the functions on it, for example
```python
import cpadlib.cryptolib as cryptolib

from cpadlib import cryptolib
if __name__ == "__main__":
    #Text to cipher
    text= input("Enter a text to encrypt\n> ")
    #One time pad
    otp = cryptolib.OneTimePadGen(len(text))
    #One time pad copy
    otp_dup = otp.copy()
    #Everytime we call the cipher or decipher function, we should always call
    #it like this, because these two functions returns a tuple containing the
    #string ciphered and the one time pad without the characters that you used.
    #   If you do not call the cipher/decipher function like this, be 100% sure that
    #you will clear the characters used from the otp.
    print("Before encipher")
    print("One Time Pad: {}\nOne Time Pad Duplicate: {}\nText to encrypt: {}".format(otp,otp_dup,text))
    text_encrypted,otp = cryptolib.cipher(text,otp)
    print("\nBefore decipher\nOTP: {}\tOTP Dupe: {}\nText ciphered: {}".format(otp,otp_dup,text_encrypted))
    text_decrypted,otp_dup = cryptolib.decipher(text_encrypted,otp_dup)
    print("\nAfter encipher\nOTP: {}\tOTP Dupe: {}\nText ciphered: {}\nText deciphered: {}".format(otp,otp_dup,text_encrypted,text_decrypted))
    input("Press enter to continue...")
```
There are several functions that you can use, all of them has documentation strings, check out help(cpadlib.foolib) for further information.
